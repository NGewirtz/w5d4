Array.prototype.uniq = function() {
  const newArr = [];
  this.forEach(function(el){
    if (newArr.indexOf(el) === -1) {
      newArr.push(el);
    }
  });
  return newArr;
};

//
// console.log([1,2,3,4,5,5,5,5,1,6,4].uniq());


Array.prototype.twoSum = function() {
  const pairs = [];
  for(let i = 0; i < this.length; i++) {
    for(let j = i + 1; j < this.length; j++) {
      if(this[i] + this[j] === 0) {
        pairs.push([i,j]);
      }
    }
  }
  return pairs;
};

// console.log([1,2,3,4,-4,-1,8,-8].twoSum());


Array.prototype.transpose = function() {
  const transposed = [];
  this.forEach(function(){
    transposed.push([]);
  });
  for(let j = 0; j < this.length; j++) {
    for(let i = 0; i < this.length; i++) {
      transposed[j].push(this[i][j]);
    }
  }
  return transposed;
};

// console.log([[1,2,3],[4,5,6],[7,8,9]].transpose());


Array.prototype.myEach = function(callback) {
  for(let i = 0; i < this.length; i++) {
    callback(this[i]);
  }
};


let arr = [1,2,3];

// arr.myEach(function(el){
//   console.log(`this is my ${el}`);
// });



Array.prototype.myMap = function(callback) {
  const returnArr = [];
  for(let i = 0; i < this.length; i++) {
    returnArr.push(callback(this[i]));
  }
  return returnArr;
};

let x = arr.myMap(function(el){
  return `this is my ${el}`;
});

// console.log(x);


Array.prototype.myReduce = function(callback, val) {
  if (val) {
    var acc = val;
    var arr = this;
  } else {
    var acc = this[0];
    var arr = this.slice(1);
  }

  for(let i = 0; i < arr.length; i ++) {
    acc = callback(acc, arr[i]);
  }
  return acc;

};

// console.log([1, 2, 3].myReduce(function(acc, el) {
//   return acc + el;
// }));
//
//
// console.log([1, 2, 3].myReduce(function(acc, el) {
//   return acc + el;
// }, 25));



Array.prototype.bubbleSort = function() {
  let arr = this;
  let unsorted = true;
  while(unsorted) {
    unsorted = false;
    for(let i = 0; i < arr.length-1; i++) {
      if(arr[i] > arr[i+1]) {
        let bigger = arr[i];
        let smaller = arr[i+1];
        arr[i] = smaller;
        arr[i+1] = bigger;
        unsorted = true;
      }
    }
  }
  return arr;
};

// console.log([4,2,5346576,123,1,2,3,6,11,3,-7,11].bubbleSort());


String.prototype.subStrings = function() {
  const subStrs = [];
  const str = this;
  for(let i = 0; i < this.length; i++) {
    for(let j = this.length; j > 0; j-- ) {
      if (str.slice(i,j)) {
        subStrs.push(str.slice(i,j));
      }
    }
  }
  return subStrs;
};
//
// console.log('racecar'.subStrings());


Array.prototype.range = function(start,end) {
  if (start === end){
    return [this[start]];
  }
  return [this[start]].concat(this.range(start+1,end));
};

// console.log(["I","go","to","the",'store','at','eleven'].range(2,6));
// console.log([1,2,3,4,5,6,7,8,9,10].range(2,4));

function sumRec(arr) {
  if (arr.length <= 1 ) {
    return arr[0];
  }
  return (arr[0] + sumRec(arr.slice(1)));
}

// console.log(sumRec([1,2,3,4,5,6,7,8,9,10]));


function exponent(base, exp) {
  if (exp === 0) {
    return 1;
  }
  return base * exponent(base, exp-1);
}

// console.log(exponent(3,3));

function exponent2(base, exp) {
  if (exp === 0) {
    return 1;
  }else if (exp === 1) {
    return base;
  }
  if (exp % 2 === 0) {
    return exponent2(base, exp/2) ** 2;
  }else {
    return base * (exponent2(base, (exp-1)/2) ** 2);
  }
}

// console.log(exponent2(2,3));


function fibs(n) {
  if (n === 1) {
    return [0]
  } else if (n === 2) {
    return [0,1]
  }
  let fibArr = fibs(n-1)
  fibArr.push(fibArr[fibArr.length-1] + fibArr[fibArr.length-2])
  return fibArr
}

// console.log(fibs(15))



function bsearch(arr, target) {
  if (arr.length === 0) {
    return -1
  }
  let midIdx = Math.floor(arr.length/2);
  if (arr[midIdx] === target ) {
    return midIdx
  }else if (arr[midIdx] > target) {
    return bsearch(arr.slice(0, midIdx), target)
  }else{
    let subAns = bsearch(arr.slice(midIdx+1), target)
    if (subAns === -1) {
      return -1
    } else {
      return subAns + 1 + midIdx
    }
  }
}


// console.log(bsearch([1,4,9,12,21,28,33,39,40,41,43,49], 9));
// console.log(bsearch([1,4,9,12,21,28,33,39,40,41,43,49], 1));
// console.log(bsearch([1,4,9,12,21,28,33,39,40,41,43,49], 41));
// console.log(bsearch([1,4,9,12,21,28,33,39,40,41,43,49], 55));



function mergeSort(arr) {
  if (arr.length <= 1) {
    return arr;
  }
  let midIdx = Math.floor(arr.length/2);
  let sorted_left = mergeSort(arr.slice(0, midIdx));
  let sorted_right = mergeSort(arr.slice(midIdx));

  return merge(sorted_left, sorted_right);
}


function merge(left, right) {
  const returnArr = [];
  while( left.length > 0 && right.length > 0) {
    if (left[0] <= right[0]) {
      returnArr.push(left[0]);
      left.shift();
    }else {
      returnArr.push(right[0]);
      right.shift();
    }
  }
  return returnArr.concat(left).concat(right)
}

// console.log(mergeSort([4,2,123,2,3,6,11,3,11]));




function subsets(arr) {
  if (arr.length === 0) {
    return [[]]
  }

  let firstEl = arr[0]
  let subs = subsets(arr.slice(1))
  let newSubs = subs.map(function(el) {
    return `${el} ${firstEl}`
  });
  return subs.concat(newSubs);
}

// console.log(subsets([1,2,3]));



function Cat(name, owner) {
  this.name = name;
  this.owner = owner;
}

Cat.prototype.cuteStatement = function() {
  console.log(`${this.owner} loves ${this.name}`)
}

let cat1 = new Cat('apollo', 'neil')
let cat2 = new Cat('breakfast', 'john')
let cat3 = new Cat('capy', 'steve')


// cat1.cuteStatement()
// cat2.cuteStatement()

Cat.prototype.cuteStatement = function() {
  console.log(`Everyone loves ${this.name}`)
}

// cat1.cuteStatement()
// cat2.cuteStatement()

Cat.prototype.meow = function() {
  console.log('MEOWWW!!!!')
}

cat1.meow = function() {
  console.log('I dont meow')
}

// cat1.meow()
// cat2.meow()


function Student(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.courses = [];
}

Student.prototype.name = function() {
  return `${firstName} ${lastName}`
}

Student.prototype.enroll = function(course) {
  this.hasConflict(course);
  if (!this.courses.includes(course)){
    this.courses.push(course);
  }
  course.addStudent(this);
}

Student.prototype.courseLoad = function() {
  const hash = {};
  this.courses.forEach(function(course){
    hash[course.department] =  (hash[course.department] || 0) + course.numOfCredits;
  })
  return hash;
}

Student.prototype.hasConflict = function(newCourse) {
  this.courses.forEach(function(curCourse){
    if (curCourse.conflictsWith(newCourse)) {
      throw "Conflict detected"
    }
  })
}


function Course(name, department, num, timeBlock, daysOfWeek) {
  this.name = name;
  this.department = department;
  this.numOfCredits = num;
  this.students = [];
  this.timeBlock = timeBlock;
  this.daysOfWeek = daysOfWeek;
}

Course.prototype.addStudent = function(student) {
  this.students.push(student)
}

Course.prototype.conflictsWith = function(course) {
  if ((this.timeBlock === course.timeBlock) &&
    (this.daysOfWeek.length != (this.daysOfWeek - course.daysOfWeek).length) ) {
    return true
  }
  return false
}


let student1 = new Student('neil', 'g')
let student2 = new Student('david', 'w')
let student3 = new Student('jose', 'r')

let course1 = new Course('physics','science', 5, 1, ['mon', 'thurs'])
let course2 = new Course('chem','science', 4, 2, ['fri'])
let course3 = new Course('bio','science', 3, 3, ['thurs'])
let course4 = new Course('alg','math', 3, 2,['tues'])
let course5 = new Course('calc','math', 5, 1, ['mon', 'wed'])


student1.enroll(course1)
student1.enroll(course2)
student1.enroll(course5)
student2.enroll(course1)
console.log(student1)
console.log(course1)
console.log(student1.courseLoad())
console.log(course1.conflictsWith(course5))
console.log(course5.conflictsWith(course1))
console.log(course1.conflictsWith(course2))
